package com.tarrk.intrigma.test;

import android.app.Application;
import android.content.Context;
import com.tarrk.intrigma.test.utils.LLog;

/**
 * User: Pavel
 * Date: 27.05.2016
 * Time: 12:30
 */
public class App extends Application {

    private static final String TAG = App.class.getSimpleName();

    /*************************************
     * PUBLIC STATIC FIELDS
     *************************************/
    public static App sInstance;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    @Override
    public void onCreate() {
        LLog.e(TAG, "onCreate");
        super.onCreate();
        sInstance = this;
        LLog.setDebuggable(BuildConfig.DEBUG);
    }

    /*************************************
     * PUBLIC STATIC METHODS
     *************************************/
    public static Context getContext() {
        return sInstance;
    }

}
