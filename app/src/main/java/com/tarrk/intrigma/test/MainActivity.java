package com.tarrk.intrigma.test;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.tarrk.intrigma.test.utils.LLog;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, OnDateSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final String INITIAL_DAY_OF_MONTH = "15";
    public static final String INITIAL_DAY_OF_WEEK = "THURSDAY";

    /************************************
     * BINDS
     ************************************/
    @Bind(R.id.calendar)
    MaterialCalendarView mCalendar;
    @Bind(R.id.cover_view)
    ViewGroup mCoverView;
    @Bind(R.id.day_of_month)
    TextView mDayOfMonth;
    @Bind(R.id.day_of_week)
    TextView mDayOfWeek;
    @Bind(R.id.inner_layout)
    LinearLayout mInnerLayout;
    @Bind(R.id.touch_view)
    View mTouchView;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private int mTouchX;
    private int mTouchY;

    /************************************
     * PROTECTED METHODS
     ************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LLog.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mTouchView.setOnTouchListener(this);
        mCalendar.setOnDateChangedListener(this);

        // init text views with some average values to set it height and width
        mDayOfMonth.setText(INITIAL_DAY_OF_MONTH);
        mDayOfWeek.setText(INITIAL_DAY_OF_WEEK);

    }

    @Override
    public void onBackPressed() {
        LLog.e(TAG, "onBackPressed");
        if (mCoverView.getVisibility() == View.VISIBLE) {
            hideCoverView();
        } else {
            super.onBackPressed();
        }
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        LLog.e(TAG, "onTouch - " + event.getX() + ", " + event.getY());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouchX = (int) event.getX();
                mTouchY = (int) event.getY();
                break;
        }
        return false;
    }

    @Override
    public void onDateSelected(MaterialCalendarView materialCalendarView, CalendarDay calendarDay, boolean b) {
        LLog.e(TAG, "onDateSelected");
        showCoverView(calendarDay);
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void showCoverView(CalendarDay calendarDay) {
        LLog.e(TAG, "showCoverView - " + mTouchX + ", " + mTouchY);

        // set values of the chosen date
        mDayOfMonth.setText(String.valueOf(calendarDay.getDay()));
        mDayOfWeek.setText(calendarDay.getCalendar().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));

        // change the position to put it at the center of the animation
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mInnerLayout.getLayoutParams();
        params.leftMargin = mTouchX - mInnerLayout.getWidth() / 2;
        params.topMargin = mTouchY - mInnerLayout.getHeight() / 2;
        mInnerLayout.setLayoutParams(params);

        // animate
        int finalRadius = Math.max(mCoverView.getWidth(), mCoverView.getHeight());
        Animator anim = ViewAnimationUtils.createCircularReveal(mCoverView, mTouchX, mTouchY, 0, finalRadius);
        mCoverView.setVisibility(View.VISIBLE);
        anim.start();

    }

    private void hideCoverView() {
        LLog.e(TAG, "hideCoverView");

        // init animation
        float initialRadius = (float) Math.hypot(mTouchX, mTouchY);
        Animator anim = ViewAnimationUtils.createCircularReveal(mCoverView, mTouchX, mTouchY, initialRadius, 0);

        // hide view after animation finished
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mCoverView.setVisibility(View.INVISIBLE);
            }
        });
        anim.start();
    }

}
