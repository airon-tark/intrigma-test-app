package com.tarrk.intrigma.test.utils;

import android.util.Log;

public class LLog {

    /*************************************
     * PUBLIC STATIC VARIABLES
     *************************************/
    public static final int MIN_TAG_LENGTH = 30;

    /*************************************
     * PRIVATE STATIC VARIABLES
     *************************************/
    private static boolean sDebuggable = false;
    private static String sFileName;

    /*************************************
     * PUBLIC STATIC METHODS
     *************************************/
    public static void setDebuggable(boolean isDebuggable) {
        sDebuggable = isDebuggable;
    }

    public static void e(Object o, String message) {
        if (sDebuggable && message != null) {
            Log.e(createTag(o), message);
        }
    }

    /*************************************
     * PRIVATE STATIC METHODS
     *************************************/
    private static String createTag(Object object) {
        if (object instanceof String) {
            return padStart((String) object, MIN_TAG_LENGTH);
        } else {
            return createTag(object.getClass());
        }
    }

    private static String createTag(Class clazz) {
        //return clazz.getSimpleName().isEmpty() ? clazz.getName() : clazz.getSimpleName();
        return clazz.getSimpleName().isEmpty() ? clazz.getName() : padStart(clazz.getSimpleName(), MIN_TAG_LENGTH);
    }

    public static String padStart(String s, int length) {
        /**
         * Pavel: changed  to "-->" and move it to tag to better looking in IDEA output
         */
        StringBuilder sb = new StringBuilder();
        sb.append("-->");
        for (int i = 0; i < length - s.length(); i++) {
            sb.append("\u0020");
        }
        sb.append(s);
        return new String(sb);
    }

}
